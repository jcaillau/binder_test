# binder_test

# Jupyter Notebook for the Smooth Case Example (repeated test)
Click on the following picture in order to launch the notebook:

Jupyter:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fjcaillau%2Fbinder_test.git/master?filepath=smooth_case.ipynb)

Jupyter lab:
* smooth_case
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fjcaillau%2Fbinder_test.git/master?urlpath=lab/tree/smooth_case.ipynb)
* kepler
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fjcaillau%2Fbinder_test.git/master?urlpath=lab/tree/kepler.ipynb)
