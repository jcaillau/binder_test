from jax.config import config
config.update("jax_enable_x64", True) # *** at startup - restart runtime, if necessary ***
import jax.numpy as np
from jax import grad, jacfwd, jacrev
import ivp
from ivp import exp, expd
import nle

class OCP:
     """OptimalControlProblem, Lagrange type
     """

     def __init__(self, cost, dyn):
       """o = OCP(cost, dyn)
       """ # add constraints, other cost types (child classes...) - See BOCOP

       self.cost = cost
       self.dyn = dyn

     @property
     def cost(self):
       """c = cost(t, x, p, *pars)

       Lagrange cost value
       """
       return self.__cost

     @cost.setter
     def cost(self, cost):
       self.__cost = cost

     @property
     def dyn(self):
       """dx = dyn(t, x, p, *pars)

       Dynamics value
       """
       return self.__dyn

     @dyn.setter
     def dyn(self, dyn):
       self.__dyn = dyn

class Hamiltonian:
    """Hamiltonian and its derivatives
    """

    def __init__(self, fun, grad=None, hess=None, npars=None):
        """h = Hamiltonian(val, grad=None, hess=None)
        
        When grad is None, AD is called on fun. When hess if None, AD is called on grad (user defined, or AD computed).
        """
        self.__val = fun
        if npars is None:
            self.__npars = fun.__code__.co_argcount - 3 # fun = h(t, x, p, par1, par2, ...)
        else:
            self.__npars = npars # fromOCP
        if grad is None:
            foo = jacrev(fun, argnums=tuple(range(0, 3 + self.__npars)))
            self.__grad = lambda t, x, p, *pars: np.hstack(foo(t, x, p, *pars))
        else: self.__grad = grad
        if hess is None:
            fooo = jacfwd(self.__grad, argnums=tuple(range(0, 3 + self.__npars)))
            self.__hess = lambda t, x, p, *pars: np.column_stack(fooo(t, x, p, *pars))
        else: self.__hess = hess

    @classmethod
    def fromC(cls, filename):
        """h = Hamiltonian.fromC(filename)

        Generates Hamiltonian, gradient, hessian from C file
        """
        raise Unimplemented

    @classmethod
    def fromF(cls, filename):
        """h = Hamiltonian.fromF(filename)

        Generates Hamiltonian, gradient, hessian from Fortran file
        """
        raise Unimplemented

    @classmethod
    def fromOCP(cls, o, fun, pcost=-1.):
        """h = Hamiltonian.fromOCP(o, fun, pcost)
        
        Generates Hamiltonian from OCP o and dynamic feedback control fun.
        Default normal case (pcost=-1.)
        """
        npars = fun.__code__.co_argcount - 3 # fun = u(t, x, p, par1, par2, ...); must be the same for o.cost and o.dyn
        def hfun(t, x, p, *pars):
            u = fun(t, x, p, *pars)
            h = pcost*o.cost(t, x, u, *pars) + np.dot(p, o.dyn(t, x, u, *pars))
            return h
        return cls(hfun, npars=npars)

    @property
    def val(self):
        """h = val(t, x, p, *pars)

        Hamiltonian value
        """
        return self.__val

    @property
    def grad(self):
        """v = grad(t, x, p, *pars)

        Gradient of the Hamiltonian
        """
        return self.__grad

    @property
    def hess(self):
        """H = hess(t, x, p, *pars)

        Hessian of the Hamiltonian
        """
        return self.__hess

    def vec(self, t, x, p, *pars):
        """v = vec(t, x, p, *pars)

        Symplectic gradient of the Hamiltonian
        """
        n = x.shape[0]
        v = self.grad(t, x, p, *pars)
        v = np.hstack((v[1+n:1+2*n], -v[1:1+n]))
        return v
    
    def dvec(self, t, x, p, *pars):
        """dv = dvec(t, x, p, *pars)
        
        Jacobian of the symplectic gradient of the Hamiltonian
        """
        n = x.shape[0]
        dv = self.hess(t, x, p, *pars)
        dv = dv[1:1+2*n, 1:1+2*n]
        dv = np.vstack((dv[n:2*n, :], -dv[0:n, :]))
        return dv

class Flow:
    """Flow of Hamiltonian and derivatives of the flow
    """

    def __init__(self, h, options=None):
        """f = Flow(h, options=None)

        Generates flow from Hamiltonian. The optional options are passed to the IVP solver and can be set dynamically.
        """
        self.__h = h
        self.options = options

    @property
    def h(self):
        """h

        Hamiltonian defining the flow
        """
        return self.__h

    @property
    def options(self):
        """Options for the IVP solver.
        """
        return self.__options

    @options.setter
    def options(self, options):
        self.__options = options

    def val(self, t0, x0, p0, tf, *pars, tspan=None):
        """xf, pf = val(t0, x0, p0, tf, *pars)
        tout, xout, pout = val(t0, x0, p0, tf, *pars, tspan)

        Hamiltonian flow from (t0, x0, p0) at tf. If tspan is provided, returns all accepted steps
        when tspan=[], values on tspan otherwise (and tout=tspan).
        """
        def dyn(t, z, lpars):
            """dz = dyn(t, z, lpars)
            
            Auxiliary function (matches IVP solver interface)
            """
            n = int(z.shape[0]/2)
            x = z[0:n]
            p = z[n:2*n]
            dz = self.h.vec(t, x, p, *lpars)
            return dz
        
        n = x0.shape[0]
        z0 = np.hstack((x0, p0))
        if tspan is None:
            sol = exp(dyn, tf, t0, z0, args=(pars,), options=self.options)
            if not(sol.success): raise IVPError(sol.status, sol.message)
            zf = sol.xf
            return (zf[0:n], zf[n:2*n])
        else:
            sol = exp(dyn, tf, t0, z0, args=(pars,), time_steps=tspan, options=self.options)
            if not(sol.success): raise IVPError(sol.status, sol.message)
            tout = sol.tout
            zout = np.transpose(sol.xout)
            return (tout, zout[:, 0:n], zout[:, n:2*n])
    
    def dt0(self, t0, x0, p0, tf, *pars):
        """dxf, dpf = dt0(t0, x0, p0, tf, *pars)

        Partial derivarive wrt. t0 of the Hamiltonian flow from (t0, x0, p0) at tf.
        """
        n = x0.shape[0]
        dzf = -self.h.vec(t0, x0, p0, *pars)         
        return (dzf[0:n], dzf[n:2*n])
        
    def dtf(self, t0, x0, p0, tf, *pars):
        """dxf, dpf = dt0(t0, x0, p0, tf, *pars)

        Partial derivarive wrt. t0 of the Hamiltonian flow from (t0, x0, p0) at tf.
        """
        n = x0.shape[0]
        xf, pf = self.val(t0, x0, p0, tf, *pars)
        dzf = self.h.vec(tf, xf, pf, *pars)         
        return (dzf[0:n], dzf[n:2*n])
    
    def dx0(self, t0, x0, p0, tf, *pars):
        """dxf, dpf = dx0(t0, x0, p0, tf, *pars)

        Partial derivarive wrt. x0 of the Hamiltonian flow from (t0, x0, p0) at tf.
        """
        def dyn(t, z, lpars):
            """dz = dyn(t, z, lpars)
            
            Auxiliary function (matches IVP solver interface)
            """
            n = int(z.shape[0]/2)
            x = z[0:n]
            p = z[n:2*n]
            dz = self.h.vec(t, x, p, *lpars)
            return dz
        
        def dynd(t, z, dz, lpars):
            """dz = dynd(t, z, dz, lpars)
            
            Auxiliary function (matches IVP solver interface)
            """
            n = int(z.shape[0]/2)
            x = z[0:n]
            p = z[n:2*n]
            ddz = self.h.dvec(t, x, p, *lpars)@dz
            return ddz
                
        n = x0.shape[0]
        z0 = np.hstack((x0, p0))
        dx0 = np.eye(n)
        dp0 = np.zeros((n, n))
        dz0 = np.vstack((dx0, dp0))
        sol = expd(dyn, dynd, tf, t0, z0, dz0, args=(pars,), options=self.options)
        if not(sol.success): raise IVPError(sol.status, sol.message)
        dzf = sol.dxf            
        return (dzf[0:n, :], dzf[n:2*n, :])
        
    def dp0(self, t0, x0, p0, tf, *pars):
        """dxf, dpf = dp0(t0, x0, p0, tf, *pars)

        Partial derivarive wrt. p0 of the Hamiltonian flow from (t0, x0, p0) at tf.
        """
        def dyn(t, z, lpars):
            """dz = dyn(t, z, lpars)
            
            Auxiliary function (matches IVP solver interface)
            """
            n = int(z.shape[0]/2)
            x = z[0:n]
            p = z[n:2*n]
            dz = self.h.vec(t, x, p, *lpars)
            return dz
        
        def dynd(t, z, dz, lpars):
            """dz = dynd(t, z, dz, lpars)
            
            Auxiliary function (matches IVP solver interface)
            """
            n = int(z.shape[0]/2)
            x = z[0:n]
            p = z[n:2*n]
            ddz = self.h.dvec(t, x, p, *lpars)@dz
            return ddz
                
        n = x0.shape[0]
        z0 = np.hstack((x0, p0))
        dx0 = np.zeros((n, n))
        dp0 = np.eye(n)
        dz0 = np.vstack((dx0, dp0))
        sol = expd(dyn, dynd, tf, t0, z0, dz0, args=(pars,), options=self.options)
        if not(sol.success): raise IVPError(sol.status, sol.message)
        dzf = sol.dxf            
        return (dzf[0:n, :], dzf[n:2*n, :])
        
class Error(Exception):
    """Exceptions of the module:

    Unimplemented
    IVPError
    """
    pass

class Unimplemented(Error):
    """Raised when trying to use a (not yet) implemented feature. 
    """
    pass

class IVPError(Error):
    """Raised when IVP integration is not successful.
    
    Attributes:
        status -- termination status from IVP
        message -- explanation from IVP
    """
    def __init__(self, status, message):
        self.status = status
        self.message = message
